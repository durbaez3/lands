﻿namespace lands.ViewModels
{
    using System;
    using System.Windows.Input;

    public class MainViewModel : BaseViewModel
    {
        #region Properties
        public LoginViewModel Login
        {
            get; set;
        }

        //public LandsViewModel Lands
        //{
        //    get;
        //set;
        //}
        #endregion

        #region Constructors
        public MainViewModel()
        {

            this.Login = new LoginViewModel();
        }
        #endregion

        #region Singleton
        public static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }
            return instance;

        }
        #endregion
    }
}
